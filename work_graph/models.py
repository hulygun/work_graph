#coding:utf-8
import json
from autoslug import AutoSlugField
from django.db import models
import datetime


HOLYDAYS = (
    (1,u'Выходной'),
    (2,u'Сокращённый'),
    (3,u'Рабочий')
)
class WorkGraph(models.Model):
    name = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from='name', max_length=255, always_update=True)
    start = models.DateField()
    rule = models.TextField(blank=True,null=True,editable=False)
    holydays = models.IntegerField(max_length=1, default=1, choices=HOLYDAYS)

    def save(self, *args, **kwargs):
        self.rule  = rule_save(self)
        super(WorkGraph, self).save()

    def __unicode__(self):
        return self.name

    def get_status(self):
        data = json.loads(self.rule)
        delta = int((datetime.date.today() - self.start).days)%int(data['count'])
        if datetime.datetime.now().time() > datetime.datetime.strptime(data['elements'][delta][0], '%H:%M').time() and datetime.datetime.now().time() < datetime.datetime.strptime(data['elements'][delta][1], '%H:%M').time():
            return True
        else:
            return False

class WorkTime(models.Model):
    start = models.TimeField()
    stop = models.TimeField()

    def __unicode__(self):
        if self.start == self.stop:
            return u'Выходной'
        else:
            return u'%s - %s'%(self.start, self.stop)

class CycleElement(models.Model):
    graph = models.ForeignKey(WorkGraph)
    time = models.ForeignKey(WorkTime)
    priority = models.IntegerField(max_length=2)
    def __unicode__(self):
        return u'%s (%s)'%(self.priority, self.time)

class Holyday(models.Model):
    name = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from='name', max_length=255, always_update=True)
    date = models.DateField()
    const = models.BooleanField(default=True)

def rule_save(model):
    elements = CycleElement.objects.filter(graph=model).order_by('priority').select_related('time')
    count = elements.count()
    li = []
    for item in elements:
        li.append([datetime.time.strftime(item.time.start,'%H:%M'), datetime.time.strftime(item.time.stop,'%H:%M')])
    out = {'count':count, 'elements':li}
    res = json.dumps(out)
    return res