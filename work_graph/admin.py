from django.contrib import admin
from models import *

class InlineElement(admin.TabularInline):
    model = CycleElement
    extra = 0
class AdminGraph(admin.ModelAdmin):
    inlines = [InlineElement]
admin.site.register(WorkGraph, AdminGraph)
admin.site.register(WorkTime)
admin.site.register(Holyday)
